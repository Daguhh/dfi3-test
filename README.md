## Fonctionnalités :

 - Ouverture sur un menu (à définir dans *config*, [NaifMenu](https://framagit.org/Daguhh/naifmenu) par défaut)
 - Présentation des applications ouvertes sous forme d'onglets
 - Fermeture des onglets d'un simple clic droit sur la barre titre
 - Affichage à deux panneaux (avec NaïfMenu)

## Installation :

#### Dépendances :
**requises**:

- paquets debian
```bash
apt install i3 wmctrl
```
- menu NaïfMenu
```bash
wget https://framagit.org/Daguhh/naifmenu/uploads/866b957c253ae6ffc940754d2ed00c26/naifmenu_0.1_amd64.deb
dpkg -i naifmenu_0.1_amd64.deb
```

**optionnelles**:

- transparence
```
apt install compton
```

#### Configurations

Copiez le fichier *config* dans *~/.config/i3/config*
Editer si necessaire


## Activation :

Soit

Lancer i3 dans le gestionnaire de connection 

ou

Pour passer à i3 en conservant les configurations de l'environnement xfce debian-facile

```bash
chmod +x toogle_Xfi3.sh
./toogle_Xfi3.sh
```


