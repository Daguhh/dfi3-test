#!/bin/bash


actual_wm=$(wmctrl -m | grep Name | awk '{print $2}')
echo $actual_wm

if [[ $actual_wm == "i3" ]]; then
    echo "vous utiliez i3, retour à xfwm4"
    $(xfconf-query -c xfce4-session -p /sessions/Failsafe/Client0_Command -t string -s "xfwm4" -t string -s "--replace" -a)

elif [[ $actual_wm == "Xfwm4" ]]; then
    echo "vous utilisez xfwm4, retour à i3"
    $(xfconf-query -c xfce4-session -p /sessions/Failsafe/Client0_Command -t string -sa i3)
fi
